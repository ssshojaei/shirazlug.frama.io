---
draft: false
title: "لینک‌های مفید"
subtitle: "دسترسی به لینک‌های مفید شیرازلاگ"
description: "دسترسی به لینک‌های مفید شیرازلاگ"
keywords: ["شیرازلاگ", "گنو/لینوکس", "متن‌باز", "نرم‌افزار آزاد", "لینوکس"]
# date: "2020-08-10T15:12:46+04:30"
readmore: true
---

##### گروه کاربران لینوکس شیراز

- [کانال تلگرام](https://t.me/shirazlug)
- [گروه تلگرام جهت پرسش و پاسخ](https://tiny.cc/shiraz)
- [اینستاگرام](https://www.instagram.com/ShirazLUG.ir/)
- [پروژه‌های شیرازلاگ](https://framagit.org/shirazlug)

---

##### گروه تلگرام سایر لاگ‌ها در ایران
- [تهران](https://tiny.cc/teh)
- [اصفهان](https://tiny.cc/esfahan)
- [اراک](https://tiny.cc/arak)
- [اهواز](https://tiny.cc/ahwaz)
- [زنجان](https://tiny.cc/zanjan)
- [قزوین](https://tiny.cc/qazvin)
- [قم](https://tiny.cc/Qom)
- [کرمانشاه](https://tiny.cc/kermanshah)
- [گیلان](https://tiny.cc/guilan)
- [مشهد](https://tiny.cc/mashad)
- [مازندران](https://tiny.cc/mazandaran)
- [گرگان](https://t.me/Gorganlug)
- [همدان](https://tiny.cc/hmdlug)
