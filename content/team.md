---
draft: false
title: "تیم اجرایی"
subtitle: "معرفی راهبران و تیم اجرایی شیرازلاگ"
keywords: "شیرازلاگ,تیم اجرایی، راهبران شیرازلاگ، گرافیک، توسعه دهنده سایت،رادیولاگ، شبکه‌های اجتماعی"
description: "معرفی راهبران و تیم اجرایی شیرازلاگ"
date: "1397-08-09"
readmore: true
---
## راهبران منتخب شیرازلاگ در سال ۹۹
[![امین خزاعی](/img/team/khozaei.svg)](/members/khozaei/)
[![مریم بهزادی](/img/team/behzadi.svg)](/members/behzadi/)
[![محمد میرشائی](/img/team/mirshaei.svg)](/members/mirshaei/)

---

## گرافیک
[![مریم بهزادی](/img/team/behzadi.svg)](/members/behzadi/)
[![محسن نظام‌الملکی](/img/team/nezam.svg)](/members/nezam/)
[![امین خزاعی](/img/team/khozaei.svg)](/members/khozaei/)

---

## توسعهٔ سایت
[![مریم بهزادی](/img/team/behzadi.svg)](/members/behzadi/)
[![امین خزاعی](/img/team/khozaei.svg)](/members/khozaei/)
[![محمد میرشائی](/img/team/mirshaei.svg)](/members/mirshaei/)
[![محسن نظام‌الملکی](/img/team/nezam.svg)](/members/nezam/)
![وجیهه نیکخواه](/img/team/nikkhah.svg)
![پویا برزگر](/img/team/barzegar.svg)
![زهره بیضاوی](/img/team/beyzavi.svg)
[![بابک رزمجو](/img/team/razmjoo.svg)](/members/razmjoo/)
[![شهرام شایگانی](/img/team/shaygani.svg)](/members/shaygani/)

---

## رادیولاگ
![مهدی مهاجر](/img/team/mohajer.svg)

---

## شبکه‌های اجتماعی
![وجیهه نیکخواه](/img/team/nikkhah.svg)
![محمد شعاعی](/img/team/shoaei.svg)
[![محمد میرشائی](/img/team/mirshaei.svg)](/members/mirshaei/)
[![مریم بهزادی](/img/team/behzadi.svg)](/members/behzadi/)

---

## برنامه‌ریزی و هماهنگی جلسات
[![مریم بهزادی](/img/team/behzadi.svg)](/members/behzadi/)
[![امین خزاعی](/img/team/khozaei.svg)](/members/khozaei/)
![محمد شعاعی](/img/team/shoaei.svg)

---